#!/usr/bin/env python3

import random
from argparse import ArgumentParser
from pathlib import Path
from getpass import getuser

# TODO: get options
# - wander -> check timestamp?
# - refactor stuff
# - name the cat?
# - pet the cat?

home_dir = "/home/"
porch_dir = "/porch/"
bowl_dir = "/porch/bowl/"
old_bowl_dir = '/.bowl'

cat = '''
  |\      _,,,---,,_
  /,`.-'`'    -.  ;-;;,_
 |,4-  ) )-,_..;\ (  `'-'
'---''(_/--'  `-'\_)
'''
cat_name = 'simon'
cat_pronoun = 'he'

def get_porches():
    porches = Path(home_dir).glob("*" + porch_dir)
    return sorted([str(p.parts[2]) for p in porches])

def print_porches(porches):
    print("Neighborhood porches")
    for porch in porches:
        print(" * " + porch)

def get_eats(porch):
    eats = Path(f"{home_dir}{porch}{bowl_dir}").glob("*")
    return list(eats)

def get_porches_with_eats():
    porches = get_porches()
    out = []
    for porch in porches:
        eats = get_eats(porch)
        if len(eats) > 0:
            out.append(eats)
    return out

def wander():
    print(f"off {cat_pronoun} goes!")

    # remove him from the current porch
    cat_location = locate()
    if cat_location:
        cat_location.unlink()

    # get all the food that's out there
    porches_with_eats = get_porches_with_eats()

    # no food :(
    if len(porches_with_eats) == 0:
        return

    # get where we're going and what we're eating
    random.shuffle(porches_with_eats)
    eats_list = porches_with_eats[0]
    random.shuffle(eats_list)
    target_eats = eats_list[0]
    target_porch = target_eats.parent.parent

    # go to the porch
    new_cat_location = target_porch / cat_name
    with open(str(new_cat_location), "w") as fil:
        fil.write(cat)
        fil.write(f"thank you for the {target_eats.stem}!\n")

    # eat the food
    target_eats.unlink()

def locate():
    cat_porches = list(Path(home_dir).glob("*" + porch_dir + cat_name))
    if len(cat_porches) == 0:
        return None
    else:
        return cat_porches[0]

def get_bowl_path():
    return Path(home_dir + getuser() + bowl_dir)

def init():
    # create the new stuff
    bowl_path = get_bowl_path()
    bowl_path.parent.mkdir(exist_ok=True)
    bowl_path.parent.chmod(0o777)
    bowl_path.mkdir(exist_ok=True)
    bowl_path.chmod(0o777)

    # get rid of the old stuff, if applicable
    old_bowl = Path(home_dir + getuser() + old_bowl_dir)
    if not old_bowl.exists():
        return

    for food in old_bowl.iterdir():
        new_food = bowl_path / food.stem
        food.rename(new_food)

    old_bowl.rmdir()

    print("You're all set up!")

def feed(food):
    # if no bowl, bail
    bowl_path = get_bowl_path()
    if not bowl_path.exists():
        print("you've got no bowl to put food in! try --init first")
        return

    food_path = bowl_path / food
    if food_path.exists():
        print(f"you already have {food} in your bowl!")
        return

    food_path.touch()
    print(f"you left some {food} for {cat_name}!")

def main():
    parser = ArgumentParser()
    parser.add_argument('-l', '--locate', action='store_true', help="locate the neighborcat")
    parser.add_argument('-i', '--init', action='store_true', help="initialize your porch")
    parser.add_argument('-f', '--feed', help="leave something in the bowl")
    parser.add_argument('-w', '--wander', action='store_true', help="set the cat a-wandering")
    args = parser.parse_args()

    if args.locate:
        porch = locate()
        if porch:
            print(f"last I saw {cat_name}, {cat_pronoun} was on {porch.parts[2]}'s porch")
        else:
            print(f"hmm, {cat_pronoun} must have wandered off. maybe you should leave some food?")
            
    elif args.init:
        init()
    elif args.wander:
        wander()
    elif args.feed:
        feed(args.feed)
    else:
        print_porches(get_porches())

if __name__ == "__main__":
    main()
